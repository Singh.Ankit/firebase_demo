package com.example.firebase_demo;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
     String REG_TOKEN= "REG_TOKEN";
    @Override
    public void onTokenRefresh() {
        String recent_Token= FirebaseInstanceId.getInstance().getToken();
        Log.d(REG_TOKEN,recent_Token);
    }
}
