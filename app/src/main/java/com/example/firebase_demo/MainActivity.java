package com.example.firebase_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
    EditText etData;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etData=findViewById(R.id.editText);
        submit=findViewById(R.id.button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txt_data=etData.getText().toString();
                if(txt_data.isEmpty())
                {
                    Toast.makeText(MainActivity.this,"No data entered",Toast.LENGTH_LONG).show();
                }
                else
                {
                    FirebaseDatabase.getInstance().getReference().child("Testing1").push().child("Data").setValue(txt_data);
                    Toast.makeText(MainActivity.this,"Data Submitted",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
